module Main where

import Prelude

import Control.Alt (class Alt, (<|>))
import Control.Alternative (class Alternative)
import Control.Lazy (class Lazy, fix)
import Control.Plus (class Plus)
import Data.Array (many, replicate)
import Data.Foldable (elem, fold, notElem)
import Data.Generic.Rep (class Generic)
import Data.Generic.Rep.Show (genericShow)
import Data.Maybe (Maybe(..))
import Data.String.CodeUnits (singleton, toCharArray, uncons)
import Data.Tuple (Tuple(..))
import Global (readFloat)

-- AST

data JsonValue = JsonNull
  | JsonBoolean Boolean
  | JsonNumber Number
  | JsonString String
  | JsonArray (Array JsonValue)
  | JsonObject (Array (Tuple String JsonValue))

derive instance genericJsonValue :: Generic JsonValue _

instance showJsonValue :: Show JsonValue where
  show x = genericShow x

-- Parser

newtype Parser a = Parser (String -> Maybe (Tuple String a))

instance functorParser :: Functor Parser where
  map f (Parser parse) = Parser \input -> do
    Tuple input' x <- parse input
    pure $ Tuple input' $ f x

instance applyParser :: Apply Parser where
  apply (Parser parseF) (Parser parseX) = Parser \input -> do
    Tuple input' f <- parseF input
    Tuple input'' x <- parseX input'
    pure $ Tuple input'' $ f x

instance applicativeParser :: Applicative Parser where
  pure x = Parser \input -> Just $ Tuple input x

instance altParser :: Alt Parser where
  alt (Parser parse1) (Parser parse2) = Parser \input ->
    parse1 input <|> parse2 input

instance plusParser :: Plus Parser where
  empty = Parser $ const Nothing

instance alternativeParser :: Alternative Parser

instance semigroupParser :: Semigroup s => Semigroup (Parser s) where
  append (Parser parse1) (Parser parse2) = Parser \input -> do
    Tuple input' x <- parse1 input
    Tuple input'' y <- parse2 input'
    pure $ Tuple input'' $ x <> y

instance monoidParser :: Monoid m => Monoid (Parser m) where
  mempty = Parser \input -> Just $ Tuple input mempty

instance lazyParser :: Lazy (Parser a) where
  defer get = Parser \input ->
    runParser (get unit) input

runParser :: forall a. Parser a -> String -> Maybe (Tuple String a)
runParser (Parser parse) = parse

-- Utils

charParser :: (Char -> Boolean) -> Parser String
charParser match = Parser \input ->
  case uncons input of
    Just { head, tail } | match head -> Just $ Tuple tail $ singleton head
    _ -> Nothing

charSetParser :: Array Char -> Parser String
charSetParser charSet = charParser \char -> elem char charSet

stringParser :: String -> Parser String
stringParser string =
  let exactCharParser x = charParser $ (==) x
  in fold $ exactCharParser <$> toCharArray string

zeroOrOne :: forall a. Monoid a => Parser a -> Parser a
zeroOrOne parser = parser <|> mempty

zeroOrMore :: forall a. Monoid a => Parser a -> Parser a
zeroOrMore parser = fold <$> many parser

oneOrMore :: forall a. Monoid a => Parser a -> Parser a
oneOrMore parser = parser <> zeroOrMore parser

separatedBy :: forall a b. Parser a -> Parser b -> Parser (Array b)
separatedBy separatorParser elementParser =
  let
    singleElementParser = pure <$> elementParser
    repeatedElementParser = zeroOrMore $ separatorParser *> singleElementParser
  in zeroOrOne $ singleElementParser <> repeatedElementParser

-- JSON

jsonNullParser :: Parser JsonValue
jsonNullParser = const JsonNull <$> stringParser "null"

jsonBooleanParser :: Parser JsonValue
jsonBooleanParser = JsonBoolean <$> (==) "true" <$> (stringParser "true" <|> stringParser "false")

jsonNumberParser :: Parser JsonValue
jsonNumberParser = JsonNumber <$> readFloat <$>
  let
    digitParser = charParser $ between '0' '9'
    positiveInteger = (charParser $ between '1' '9') <> zeroOrMore digitParser
    integerParser = (zeroOrOne $ stringParser "-") <> (stringParser "0" <|> positiveInteger)
    fractionParser = stringParser "." <> oneOrMore digitParser
    exponentParser = charSetParser ['e', 'E'] <> (zeroOrOne $ charSetParser ['+', '-']) <> oneOrMore digitParser
  in integerParser <> zeroOrOne fractionParser <> zeroOrOne exponentParser

stringLiteralParser :: Parser String
stringLiteralParser =
  let
    inQuotes parser = stringParser "\"" *> parser <* stringParser "\""
    nonSpecialCharParser = charParser \x -> x >= '\x20' && notElem x ['\x7f', '\\', '"']
    specialCharParser = stringParser "\\" <> charSetParser ['"', '\\', '/', 'b', 'f', 'n', 'r', 't']
    hexDigitParser = charParser \x -> between '0' '9' x || between 'a' 'f' x || between 'A' 'F' x
    unicodeCharParser = stringParser "\\u" <> (fold $ replicate 4 hexDigitParser)
    innerCharParser = nonSpecialCharParser <|> specialCharParser <|> unicodeCharParser
  in inQuotes $ zeroOrMore innerCharParser

jsonStringParser :: Parser JsonValue
jsonStringParser = JsonString <$> stringLiteralParser

pad :: forall a. Parser a -> Parser a
pad parser =
  let whitespaceParser = zeroOrMore $ charSetParser [' ', '\t', '\n', '\r']
  in whitespaceParser *> parser <* whitespaceParser

jsonArrayParser :: Parser JsonValue -> Parser JsonValue
jsonArrayParser jsonValueParser = JsonArray <$>
  let
    inBrackets parser = stringParser "[" *> parser <* stringParser "]"
    elementsParser = separatedBy (stringParser ",") jsonValueParser
  in inBrackets $ pad elementsParser

jsonObjectParser :: Parser JsonValue -> Parser JsonValue
jsonObjectParser jsonValueParser = JsonObject <$>
  let
    inBraces parser = stringParser "{" *> parser <* stringParser "}"
    keyParser = pad stringLiteralParser <> stringParser ":"
    pairParser = Tuple <$> keyParser <*> jsonValueParser
    pairsParser = separatedBy (stringParser ",") pairParser
  in inBraces $ pad pairsParser

jsonValueParser :: Parser JsonValue
jsonValueParser = fix \jsonValueParser -> pad $ jsonNullParser
  <|> jsonBooleanParser
  <|> jsonNumberParser
  <|> jsonStringParser
  <|> jsonArrayParser jsonValueParser
  <|> jsonObjectParser jsonValueParser

